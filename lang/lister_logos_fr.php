<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/lister_logos.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'controle_max_height' => 'Trop hauts',
	'controle_max_height_explication' => 'Les logos ne doivent pas faire plus de @nb@ pixels de hauteur. Cette valeur a été configurée par le webmestre du site. Veuillez réduire leur hauteur le cas échéant, s’il vous plaît.',
	'controle_max_size' => 'Trop lourds',
	'controle_max_size_explication' => 'Les logos ne doivent pas peser plus de @nb@ ko. Cette valeur a été configurée par le webmestre du site. Veuillez réduire le poids de ces logos le cas échéant, s’il vous plaît.',
	'controle_max_width' => 'Trop larges',
	'controle_max_width_explication' => 'Les logos ne doivent pas faire plus de @nb@ pixels de largeur. Cette valeur a été configurée par le webmestre du site. Veuillez réduire leur largeur le cas échéant, s’il vous plaît.',
	'controle_min_height' => 'Pas assez hauts',
	'controle_min_height_explication' => 'Les logos ne doivent pas faire moins de @nb@ pixels de hauteur. Cette valeur a été configurée par le webmestre du site. Veuillez réduire leur hauteur le cas échéant, s’il vous plaît.',
	'controle_min_width' => 'Pas assez larges',
	'controle_min_width_explication' => 'Les logos ne doivent pas faire moins de @nb@ pixels de largeur. Cette valeur a été configurée par le webmestre du site. Veuillez réduire leur largeur le cas échéant, s’il vous plaît.',

	// I
	'info_1_logo' => 'Un logo',
	'info_logo_on_inexistant' => 'Le logo normal n’existe pas, vous pouvez supprimer celui de survol du coup.',
	'info_logos' => 'Logos',
	'info_nb_logos' => '@nb@ logos',

	// L
	'lister_logos_titre' => 'Lister les logos',
	'logo_bad' => 'Mauvais format',
	'logo_off' => 'Logo de survol',
	'logo_on' => 'Logo normal',

	// O
	'objet_supprime' => 'Objet supprimé ?',
	'objets_label' => 'Objets éditoriaux :',

	// P
	'pas_de_logos' => 'Il n’y a pas de logos pour cet objet éditorial.',
	'pas_de_logos_pas_assez_haut' => 'Il n’y a pas de logos "pas assez haut" pour cet objet éditorial.',
	'pas_de_logos_pas_assez_large' => 'Il n’y a pas de logos "pas assez large" pour cet objet éditorial.',
	'pas_de_logos_trop_haut' => 'Il n’y a pas de logos "trop haut" pour cet objet éditorial.',
	'pas_de_logos_trop_large' => 'Il n’y a pas de logos "trop large" pour cet objet éditorial.',
	'pas_de_logos_trop_lourd' => 'Il n’y a pas de logos "trop lourd" pour cet objet éditorial.',

	// T
	'titre_page' => 'Les logos',
	'titre_page_configurer_lister_logos' => 'Les logos, on en fait quoi ?',
	'titre_tous_logos' => 'Tous les logos',
	'titre_tous_logos_off' => 'Tous les logos de survol',
	'titre_tous_logos_on' => 'Tous les logos normaux'
);
