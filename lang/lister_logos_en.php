<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/lister_logos?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Example',
	'cfg_exemple_explication' => 'Explanation of this example',
	'cfg_titre_parametrages' => 'Parameters',
	'controle_max_height' => 'Too high',
	'controle_max_height_explication' => 'The logo hight cannot exceed @nb@ pixels. This value has been defined by the webmaster of this site. Please reduce if necessary.',
	'controle_max_size' => 'Too heavy',
	'controle_max_size_explication' => 'The logo file size cannot exceed @nb@ kB. This value has been defined by the webmaster of this site. Please reduce if necessary.',
	'controle_max_width' => 'Too wide',
	'controle_max_width_explication' => 'The logo width cannot exceed @nb@ pixels. This value has been defined by the webmaster of this site. Please reduce if necessary.',

	// I
	'info_1_logo' => 'One logo',
	'info_logo_on_inexistant' => 'The standard logo does not exist. You may suppress this rollover logo.',
	'info_logos' => 'Logos',
	'info_nb_logos' => '@nb@ logos',

	// L
	'lister_logos_titre' => 'List logos',
	'logo_bad' => 'Bad format',
	'logo_off' => 'Rollover logo',
	'logo_on' => 'Standard logo',

	// O
	'objet_supprime' => 'Object removed?',
	'objets_label' => 'Editorial objects:',

	// P
	'pas_de_logos' => 'There are no logos for this editorial object.',
	'pas_de_logos_trop_lourd' => 'There are no "too heavy" logos for this editorial object.',

	// T
	'titre_page' => 'The logos',
	'titre_page_configurer_lister_logos' => 'The logos, and where they are used?',
	'titre_tous_logos' => 'All logos',
	'titre_tous_logos_off' => 'All rollover logos',
	'titre_tous_logos_on' => 'All normal logos'
);
