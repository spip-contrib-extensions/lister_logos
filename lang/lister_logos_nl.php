<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/lister_logos?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Voorbeeld',
	'cfg_exemple_explication' => 'Uitleg van dit voorbeeld',
	'cfg_titre_parametrages' => 'Parameters',
	'controle_max_height' => 'Te hoog',
	'controle_max_height_explication' => 'De logo’s mogen niet meer dan @nb@ pixels hoog zijn. Deze waarde werd door de webmaster vastgelegd. Verklein het logo indien nodig.',
	'controle_max_size' => 'Te zwaar',
	'controle_max_size_explication' => 'De logo’s mogen niet meer dan @nb@ kB groot zijn. Deze waarde werd door de webmaster vastgelegd. Verklein het logo indien nodig',
	'controle_max_width' => 'Te breed',
	'controle_max_width_explication' => 'De logo’s mogen niet meer dan @nb@ pixels breed zijn. Deze waarde werd door de webmaster vastgelegd. Verklein het logo indien nodig',

	// I
	'info_1_logo' => 'Een logo',
	'info_logo_on_inexistant' => 'Het normale logo bestaat niet. Je mag dit alternatieve logo verwijderen.',
	'info_logos' => 'Logo’s',
	'info_nb_logos' => '@nb@ logo’s',

	// L
	'lister_logos_titre' => 'De logo’s weergeven',
	'logo_bad' => 'Slecht formaat',
	'logo_off' => 'Alternatieve logo',
	'logo_on' => 'Standaard logo',

	// O
	'objet_supprime' => 'Verwijderd object?',
	'objets_label' => 'Editoriale objecten:',

	// P
	'pas_de_logos' => 'Er zijn geen logo’s voor dit editoriale object.',
	'pas_de_logos_trop_lourd' => 'Er zijn geen "te zware" logo’s voor dit editoriale object.',

	// T
	'titre_page' => 'De logo’s',
	'titre_page_configurer_lister_logos' => 'De logo’s, wat doen we ermee?',
	'titre_tous_logos' => 'Alle logo’s',
	'titre_tous_logos_off' => 'Alle alternatieve logo’s',
	'titre_tous_logos_on' => 'Alle normale logo’s'
);
